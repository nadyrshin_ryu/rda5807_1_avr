//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include "delay\delay.h"
#include "main.h"
#include "rda5807.h"
#include "buttmatrix.h"
#include "delay\delay.h"
#include "ledmatrix.h"
#include "max7219.h"
#include "max7219\fonts\font.h"


uint8_t Volume = 3;                     // ���������
uint8_t BassBoost = 0;                  // ��������� BassBoost
uint16_t Freq100hz = 0;                 // �������� ������� ��� ����� (��� ����� � ����������)

// ��� ����������� ��������� ��������� �����
typedef enum 
{
  Idle = 0,
  FreqSet = 1,
  SeekTune = 2,
  VolSet = 3,
  BassSet = 4
} tRadioState;

tRadioState RadioState = Idle;          // ������� ��������� �����
tRadioState RadioStateOld = Idle;       // ������� ��������� �����
uint16_t StateTickNum;                  // ������� ������� �������� ���������


//==============================================================================
// ������� �� ������� � ������� ����� ������� ������ ���������� ����� ������, 
// ������� ������ ��� ���� ������
//==============================================================================
uint16_t ButtonsPush(uint16_t Buttons, uint16_t ButtonsOld)
{
  return Buttons & (ButtonsOld ^ 0xFFFF);
}
//==============================================================================


//==============================================================================
// ��������� ������ ������� ��������� ����� � ������������� ����� ���������
// � ����� ��������� ����� � MAIN 
//==============================================================================
void RadioNewState(tRadioState NewState, uint16_t TickNum)
{
  RadioStateOld = (NewState == Idle) ? Idle : RadioState;
  RadioState = NewState;
  StateTickNum = TickNum;
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ��������� ��������� � ������� Vol x
//==============================================================================
void ShowVol(uint8_t vol)
{
  ledmatrix_fill_screenbuff(0);
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "Vol %d", vol);
  ledmatrix_update_from_buff();
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ��������� ��������� BassBoost � ������� Bass x ���
// BB x (����� ������� � 4 ������������ �������)
//==============================================================================
void ShowBass(uint8_t val)
{
  ledmatrix_fill_screenbuff(0);

#if (MAX7219_NUM == 5)
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "Bass %d", val);
#else 
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "BB %d", val);
#endif
  
  ledmatrix_update_from_buff();
}
//==============================================================================


//==============================================================================
// ��������� ������� �� ��������� �������� freq � ������� xxx.x
//==============================================================================
void ShowFreq(uint16_t freq)
{
  ledmatrix_fill_screenbuff(0);
  font_printf(ledmatrix_screenbuff, MAX7219_NUM * 8, "%d.%d", freq / 10, freq % 10);
  ledmatrix_update_from_buff();
}
//==============================================================================


//==============================================================================
// ��������� ������ �������� Freq100hz (�������� ������� ��� �����) 
// � ������������ � ����� �������� ������ 0-9
//==============================================================================
void EnterFreqDigit(uint8_t Digit)
{
  // �������� ����� ������� ������� � ���������� ���� ����� �� 1 ����� 
  // � � ������� ������ ��������� ����� �����
  uint16_t TempFreq = (Freq100hz * 10) + Digit;
  
  // � ������ ����� ����� ������� �� ������� ������������ �������
  if (TempFreq > 1080)
    TempFreq = TempFreq % 1000;     // ����������� �� ������ ������ �������
  
  Freq100hz = TempFreq;
}
//==============================================================================


//==============================================================================
// ��������� ������������ ������� ������ �� ����� ������� ������ (0->1)
//==============================================================================
void ButtonClickProcess(uint16_t ButtonClick)
{
  // ���� ������ ������ +
  if (ButtonClick & (1 << 14))
  {
    // ����������� ���������
    if (Volume <= 15)
      rda5807_SetVolume(++Volume);
    RadioNewState(VolSet, 10);
    ShowVol(Volume);
  }
  // ���� ������ ������ -
  if (ButtonClick & (1 << 15))
  {
    // ��������� ���������
    if (Volume)
      rda5807_SetVolume(--Volume);
    RadioNewState(VolSet, 10);
    ShowVol(Volume);
  }
  // ���� ������ ������ <
  if (ButtonClick & (1 << 12))
  {
    // �������� ����� ������������ ����
    rda5807_StartSeek(1);
    RadioNewState(SeekTune, 100);
  }
  // ���� ������ ������ >
  if (ButtonClick & (1 << 13))
  {
    // �������� ����� ������������ �����
    rda5807_StartSeek(0);
    RadioNewState(SeekTune, 100);
  }
  
  // ���� ������ ������ 0
  if (ButtonClick & (1 << 7))
  {
    EnterFreqDigit(0);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 1
  if (ButtonClick & (1 << 0))
  {
    EnterFreqDigit(1);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 2
  if (ButtonClick & (1 << 4))
  {
    EnterFreqDigit(2);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 3
  if (ButtonClick & (1 << 8))
  {
    EnterFreqDigit(3);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 4
  if (ButtonClick & (1 << 1))
  {
    EnterFreqDigit(4);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 5
  if (ButtonClick & (1 << 5))
  {
    EnterFreqDigit(5);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 6
  if (ButtonClick & (1 << 9))
  {
    EnterFreqDigit(6);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 7
  if (ButtonClick & (1 << 2))
  {
    EnterFreqDigit(7);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 8
  if (ButtonClick & (1 << 6))
  {
    EnterFreqDigit(8);
    RadioNewState(FreqSet, 30);
  }
  // ���� ������ ������ 9
  if (ButtonClick & (1 << 10))
  {
    EnterFreqDigit(9);
    RadioNewState(FreqSet, 30);
  }
  
  // ���� ������ ������ *
  if (ButtonClick & (1 << 3))
  {
    // ������ ��������� ������� BassBoost
    if (Freq100hz)      // ���� �������� �������� ������� ���� ����� ������� 
    {
      // ���������� ������������� rda5807 �� ����� �������
      rda5807_SetFreq_In100Khz(Freq100hz);
      Freq100hz = 0;
      RadioNewState(SeekTune, 100);
    }
  }

  // ���� ������ ������ #
  if (ButtonClick & (1 << 11))
  {
    // ������ ��������� ������� BassBoost
    BassBoost ^= 0x01;
    rda5807_SetBassBoost(BassBoost);
    RadioNewState(BassSet, 10);
    ShowBass(BassBoost);
  }
}
//==============================================================================


//==============================================================================
// MAIN
//==============================================================================
void main()
{
  uint16_t Buttons = 0, ButtonsOld = 0;
  uint16_t FreqRead = 0;
  
  // �������������� ������������ �������
  delay_ms(200);
  ledmatrix_init();
  rda5807_init();
  //rda5807_SoftReset();    // ������� ����� �����, � ����� ������� ������??
  
  // ������ ����� ��������� (���� ���� �����������)
  ledmatrix_testmatrix(1);
  delay_ms(500);
  ledmatrix_testmatrix(0);

  // ������������� ��������� rda5807 ��-���������
  rda5807_SetupDefault();
  rda5807_SetVolume(Volume);
  rda5807_SetBassBoost(BassBoost);

  delay_ms(100);

  // ������������� ��������� �������
  rda5807_SetFreq_In100Khz(1012);
  // ��������� � ��������� �������� ������/��������� �������
  RadioNewState(SeekTune, 100);

  while (1)
  {
    // ��������� ���������� � ���������� ������ ������� �� ������
    ButtonsOld = Buttons;
    Buttons = buttmatrix_scan();
    uint16_t ButtonClick = ButtonsPush(Buttons, ButtonsOld);
    ButtonClickProcess(ButtonClick);

    // ������������ ������ �������� ��������� ����� (RadioState)
    switch (RadioState)
    {
      // ��������� �������� (�������������)
    case Idle:
      break;
      // ��������� ����� �� ����� ����������� ���������
    case VolSet:
      break;
      // ��������� ����� �� ����� ���������/���������� BassBoost
    case BassSet:
      break;
      // ��������� ����� �� ����� ����� � ���������� �������
    case FreqSet:
      ShowFreq(Freq100hz);
      break;
      // ��������� ����� �� ����� ������ ������������
    case SeekTune:
      // ���� Seek ��� Tune � �������� - ������� �� ��������� ������� �������
      if (rda5807_Get_SeekTuneReadyFlag())
        RadioNewState(Idle, 10);

      FreqRead = rda5807_GetFreq_In100Khz();
      ShowFreq(FreqRead);
      break;
    }
    
    // ������������ ����� ��������� ����� �� ��������� ������� ���������
    if (StateTickNum)
      StateTickNum--;
    if (!StateTickNum)  // ����� ������ � ������� ��������� �������
    {
      if (RadioState != Idle)   // ������������ � ��������� ��������?
      {     
        // ������ �� rda5807 � ������� �� ��������� ������� �������
        FreqRead = rda5807_GetFreq_In100Khz();
        ShowFreq(FreqRead);
      }

      RadioNewState(Idle, 10);
      Freq100hz = 0;
    }

    // ����������� �������� ��������� �����
    delay_ms(100);
  }
}
//==============================================================================
