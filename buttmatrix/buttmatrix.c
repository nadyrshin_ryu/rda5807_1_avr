//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include "..\delay\delay.h"
#include "buttmatrix.h"
#include "i2cm.h"


#define BUTTMATRIX_SetDATA_PinMode_In(mask)   {BUTTMATRIX_DDR &= ~mask; BUTTMATRIX_Port |= mask;} 


// ������� ��� ������ �������� ����� GPIO ���������������� 
#define BUTTMATRIX_SetCols_PinMode_In(mask)   {BUTTMATRIX_ColsDDR &= ~mask; BUTTMATRIX_ColsPort |= mask;}
#define BUTTMATRIX_SetRows_PinMode_In(mask)   {BUTTMATRIX_RowsDDR &= ~mask; BUTTMATRIX_RowsPort |= mask;}
#define BUTTMATRIX_SetCols_PinMode_Out(mask)  BUTTMATRIX_ColsDDR |= mask
#define BUTTMATRIX_SetRows_PinMode_Out(mask)  BUTTMATRIX_RowsDDR |= mask
#define BUTTMATRIX_ColsPin_SET(val)           {BUTTMATRIX_ColsPort &= ~BUTTMATRIX_Cols_Mask; BUTTMATRIX_ColsPort |= val;}
#define BUTTMATRIX_RowsPin_SET(val)           {BUTTMATRIX_RowsPort &= ~BUTTMATRIX_Rows_Mask; BUTTMATRIX_RowsPort |= val;}
#define BUTTMATRIX_ColsPin_GET()              BUTTMATRIX_ColsPin
#define BUTTMATRIX_RowsPin_GET()              BUTTMATRIX_RowsPin


const uint8_t RowMasks[] = {BUTTMATRIX_Row0_Mask, BUTTMATRIX_Row1_Mask, BUTTMATRIX_Row2_Mask, BUTTMATRIX_Row3_Mask};
const uint8_t ColMasks[] = {BUTTMATRIX_Col0_Mask, BUTTMATRIX_Col1_Mask, BUTTMATRIX_Col2_Mask, BUTTMATRIX_Col3_Mask};


//==============================================================================
// ������� ��������� ��������� ������ ���������� � ���������� ��� � ���� ������� 16-��� �����
//==============================================================================
uint16_t buttmatrix_scan(void)
{
  uint16_t ReturnValue = 0;
  uint16_t ButtonsMask = 1;

  BUTTMATRIX_SetCols_PinMode_In(BUTTMATRIX_Cols_Mask);

  for (uint8_t row = 0; row < (sizeof(RowMasks) / sizeof(RowMasks[0])); row++)
  {
    uint16_t RowMask = RowMasks[row];
    
    BUTTMATRIX_SetRows_PinMode_In(BUTTMATRIX_Rows_Mask);
    BUTTMATRIX_SetRows_PinMode_Out(RowMask);
    
    BUTTMATRIX_RowsPin_SET(~RowMask);
    uint16_t Cols = BUTTMATRIX_ColsPin_GET() & BUTTMATRIX_Cols_Mask;
    
    for (uint8_t col = 0; col < (sizeof(ColMasks) / sizeof(ColMasks[0])); col++)
    {
      if (!(Cols & ColMasks[col]))
        ReturnValue |= ButtonsMask;
      
      ButtonsMask <<= 1;
    }
  }
  
  return ReturnValue;
}
//==============================================================================
