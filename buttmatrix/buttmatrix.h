//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _BUTTMATRIX_H
#define _BUTTMATRIX_H

#include "../types.h"


// ���� ����������������, � ����� �������� ���������� ����� ��������
#define BUTTMATRIX_ColsPort             PORTC
#define BUTTMATRIX_ColsDDR              DDRC
#define BUTTMATRIX_ColsPin              PINC
// ���� ����������������, � ����� �������� ���������� ����� �����
#define BUTTMATRIX_RowsPort             PORTD
#define BUTTMATRIX_RowsDDR              DDRD
#define BUTTMATRIX_RowsPin              PIND


// ������� ����� �����
#define BUTTMATRIX_Col0_Mask            (1 << 0)
#define BUTTMATRIX_Col1_Mask            (1 << 1)
#define BUTTMATRIX_Col2_Mask            (1 << 2)
#define BUTTMATRIX_Col3_Mask            (1 << 3)
#define BUTTMATRIX_Row0_Mask            (1 << 0)
#define BUTTMATRIX_Row1_Mask            (1 << 1)
#define BUTTMATRIX_Row2_Mask            (1 << 2)
#define BUTTMATRIX_Row3_Mask            (1 << 3)

#define BUTTMATRIX_Rows_Mask            (BUTTMATRIX_Row0_Mask | BUTTMATRIX_Row1_Mask | BUTTMATRIX_Row2_Mask | BUTTMATRIX_Row3_Mask)
#define BUTTMATRIX_Cols_Mask            (BUTTMATRIX_Col0_Mask | BUTTMATRIX_Col1_Mask | BUTTMATRIX_Col2_Mask | BUTTMATRIX_Col3_Mask)




// ������� ��������� ��������� ������ ���������� � ���������� ��� � ���� ������� 16-��� �����
uint16_t buttmatrix_scan(void);
// ��������� ������������� ��� GPIO/I2C-����������� ��� ������ � �����������
void buttmatrix_init(void);


#endif